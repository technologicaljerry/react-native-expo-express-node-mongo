import express from 'express';

export async function createUserController(req: any, res: any) {
  try {
    const { db } = req.app;

    const { name, email, phone, address } = req.body;

    if (!name) {
      return res.status(400).json({ message: "Name is required" });
    }

    if (!email) {
      return res.status(400).json({ message: "Email is required" });
    }

    if (phone && phone.length > 10) {
      return res
        .status(400)
        .json({ message: "Phone number cannot be longer than 10 digits" });
    }

    if (address && address.length > 100) {
      return res
        .status(400)
        .json({ message: "Address must be less than 100 characters" });
    }

    // check if User exists

    const existingUser = await db.collection("users").findOne({
      email: email.toLowerCase(),
    });

    if (existingUser) {
      return res.status(400).json({ message: "User already exists" });
    }

    const result = await db.collection("users").insertOne({
      name,
      email: email.toLowerCase(),
      phone,
      address,
    });

    if (result.acknowledged) {
      res.status(200).json({ message: "User created" });
    } else {
      throw new Error("User not created");
    }
  } catch (error: any) {
    res.status(500).json({ error: error.toString() });
  }
}

import { ObjectId } from "mongodb";

export async function getUserController(req: any, res: any) {
  try {
    const { db } = req.app;

    const { id } = req.params;

    if (!id) {
      return res.status(400).json({ message: "User ID is required" });
    }

    const result = await db.collection("users").findOne({
      _id: new ObjectId(id),
    });

    if (!result) {
      return res.status(404).json({ message: "User not found" });
    }

    res.status(200).json({
      message: "User retrieved",
      user: result,
    });
  } catch (error: any) {
    res.status(500).json({ error: error.toString() });
  }
}

export async function getUsersController(req: any, res: any) {
  try {
    const { db } = req.app;

    const result = await db.collection("users").find().toArray();

    res.status(200).json({
      message: "Users retrieved",
      users: result,
    });
  } catch (error: any) {
    res.status(500).json({ error: error.toString() });
  }
}
