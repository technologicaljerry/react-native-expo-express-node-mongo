import { createUserController } from "../controllers/user.controllers";
import { getUserController } from "../controllers/user.controllers";
import { getUsersController } from "../controllers/user.controllers";

const express = require("express");

const router = express.Router();

router.get("/", getUsersController);
router.post("/", createUserController);
router.get("/:id", getUserController);

module.exports = router;
