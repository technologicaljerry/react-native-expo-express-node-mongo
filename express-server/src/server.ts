import { MongoClient } from "mongodb";
const express = require("express");
const body = require("body-parser");

const port = 5050;

async function start() {
  try {
    const app = express();

    // Create a new MongoClient
    const client = new MongoClient("mongodb://localhost:27017/user-crud", {});

    // Connect the client to the server
    await client.connect();
    console.log("Database is connected");

    // Get the database
    const db = client.db();
    app.db = db;

    // body parser
    app.use(
      body.json({
        limit: "500kb",
      })
    );

    // Routes
    app.use("/user", require("./routes/user.routes"));

    // Start server
    app.listen(port, () => {
      console.log(`Server is running on port ${port}`);
    });
  } catch (error) {
    console.error("Failed to connect to the database", error);
  }
}

start();
